if (process.env.NODE_ENV == 'development') {
    var baseUrl = 'https://dev/uat.com'      //本地地址、测试地址
} else {
    var baseUrl = 'https://prod.com'          //生产环境地址
}

export default baseUrl
