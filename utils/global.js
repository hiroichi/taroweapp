import Taro from "@tarojs/taro";

const globalData = {
    lang: 'zh',
    api_url: 'https://esthe-job.net/',
    setting: {
        skew: 0,
        rotate: 0,
        showLocation: false,
        showScale: false,
        subKey: 'CGFBZ-YRH6J-4SZFK-FXTN3-XDZPV-UTBTN',
        layerStyle: 1,
        enableZoom: true,
        enableScroll: true,
        enableRotate: false,
        showCompass: false,
        enable3D: false,
        enableOverlooking: false,
        enableSatellite: false,
        enableTraffic: false,
    },
};

export function showLoading() {
    Taro.showLoading({
        title: "正在加载中",
        mask: true,
    });
}

export function userLogin(info) {
    var c = gData();
    var globalData = c.get();
    return Taro.getUserInfo({
        success: function (res) {
            var getUser = res.userInfo;
            var userInfo = {
                nickName: getUser.nickName,
                avatarUrl: getUser.avatarUrl,
                gender: getUser.gender,
                province: getUser.province,
                city: getUser.city,
                country: getUser.country,
            };
            c.set('userinfo', userInfo);
            // console.log(globalData);
        },
    });
}

export function hideLoading() {
    Taro.hideLoading();
}

export function gTitle() {
    var c = gData();
    var globalData = c.get();
    try {
        if (typeof Taro.Current.app.config.window.navigationBarTitleText != 'undefined') {
            globalData.pagetitle = Taro.Current.app.config.window.navigationBarTitleText;
            c.set('pagetitle', globalData.pagetitle);
        }
        if (typeof Taro.Current.page.config.navigationBarTitleText != 'undefined') {
            c.set('pagetitle', Taro.Current.page.config.navigationBarTitleText);
        }
    } catch (e) { }
}
export function gData() {
    return {
        set(key, val) {
            globalData[key] = val;
        },
        get(key) {
            if (typeof key == 'undefined') {
                return globalData;
            }
            return globalData[key];
        }
    }
}
