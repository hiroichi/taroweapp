import { gData } from "../utils/global";
import Taro from "@tarojs/taro";
import { HTTP_STATUS } from '@/utilsroot/status'
import { logError } from "@/utilsroot/cb";
var urlplus = require('url-plus');

const token = '';
var api_init = function (geturl, my) {
    if (typeof my == 'undefined') {
        var c = gData();
        const globalData = c.get();
        let url = urlplus.normalize(globalData.api_url + globalData.lang + '/app/' + geturl);
        return (url + '/');
    } else {
        return urlplus.normalize(geturl);
    }
}
export default {
    baseOptions(params, method = 'GET') {
        let { url, data, my } = params
        // console.log('params', params)
        let contentType = 'application/x-www-form-urlencoded'
        contentType = params.contentType || contentType
        const option = {
            isShowLoading: true,
            loadingText: params.loadingText || '正在加载',
            url: api_init(url, my),
            data: data,
            method: method,
            header: { 'content-type': contentType, 'token': token },
            success(res) {
                if (res.statusCode === HTTP_STATUS.NOT_FOUND) {
                    return logError('api', '请求资源不存在')
                } else if (res.statusCode === HTTP_STATUS.BAD_GATEWAY) {
                    return logError('api', '服务端出现了问题')
                } else if (res.statusCode === HTTP_STATUS.FORBIDDEN) {
                    return logError('api', '没有权限访问')
                } else if (res.statusCode === HTTP_STATUS.SUCCESS) {
                    return res.data
                }
            },
            error(e) {
                logError('api', '请求接口出现问题', e)
            }
        }
        return Taro.request(option)
    },
    get(url, data = '', my) {
        let option = { url, data, my }
        return this.baseOptions(option)
    },
    post: function (url, data, contentType, my) {
        let params = { url, data, contentType, my }
        return this.baseOptions(params, 'POST')
    }
}
