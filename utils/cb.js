export const promisify = (func, ctx) => {
    // 返回一个新的function
    return function () {
        // 初始化this作用域
        var ctx = ctx || this;
        // 新方法返回的promise
        return new Promise((resolve, reject) => {
            // 调用原来的非promise方法func，绑定作用域，传参，以及callback（callback为func的最后一个参数）
            func.call(ctx, ...arguments, function () {
                // 将回调函数中的的第一个参数error单独取出
                var args = Array.prototype.map.call(arguments, item => item);
                var err = args.shift();
                // 判断是否有error
                if (err) {
                    reject(err)
                } else {
                    // 没有error则将后续参数resolve出来
                    args = args.length > 1 ? args : args[0];
                    resolve(args);
                }
            });
        })
    };
};

export const promiseImage = (url) => {
    return new Promise(function (resolve, reject) {
        resolve(url)
    })
}
export const isChinese = (str) => {
    if (escape(str).indexOf("%u") < 0) return false
    return true
}

export const handleName = (str) => {
    let res = emoj2str(str)
    if (isChinese(res)) {
        res = res.length > 4 ? res.slice(0, 4) + '...' : res
    } else {
        res = res.length > 7 ? res.slice(0, 7) + '...' : res
    }
    return res
}

export const emoj2str = (str) => {
    return unescape(escape(str).replace(/\%uD.{3}/g, ''))
}
/*获取当前页url*/
export const getCurrentPageUrl = () => {
    let pages = getCurrentPages()
    let currentPage = pages[pages.length - 1]
    let url = currentPage.route
    return url
}

export const formatTime = date => {
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()
    const hour = date.getHours()
    const minute = date.getMinutes()
    const second = date.getSeconds()

    return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

export const formatNumber = n => {
    n = n.toString()
    return n[1] ? n : '0' + n
}
export const logError = (name, action, info) => {
    if (!info) {
        info = 'empty'
    }
    try {
        let deviceInfo = wx.getSystemInfoSync()
        var device = JSON.stringify(deviceInfo)
    } catch (e) {
        console.error('not support getSystemInfoSync api', err.message)
    }
    let time = formatTime(new Date())
    console.error(time, name, action, info, device)
    // if (typeof action !== 'object') {
    // fundebug.notify(name, action, info)
    // }
    // fundebug.notifyError(info, { name, action, device, time })
    if (typeof info === 'object') {
        info = JSON.stringify(info)
    }
}

/**
 * 通用时间格式转换，将时间戳转换自己需要的格式
 *
 * [fmt] 第二参数类型字符串，注意指定
 * 年(y)、月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) ，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)
 *
 * customFormatDate(时间戳, "yyyy-MM-dd hh:mm:ss.S") 转换后 2016-07-02 08:09:04.423
 */
export const customFormatDate = (UnixTime, fmt) => {
    if (!UnixTime) return '';
    const dateTime = new Date(parseInt(`${UnixTime * 1000}`));
    const o = {
        'M+': dateTime.getMonth() + 1, //月份
        'd+': dateTime.getDate(), //日
        'h+': dateTime.getHours(), //小时
        'm+': dateTime.getMinutes(), //分
        's+': dateTime.getSeconds(), //秒
        'q+': Math.floor((dateTime.getMonth() + 3) / 3), //季度
        S: dateTime.getMilliseconds(), //毫秒
    };
    let newDataStrin = fmt || 'yyyy-MM-dd hh:mm:ss.S';
    if (/(y+)/.test(newDataStrin)) {
        newDataStrin = newDataStrin.replace(
            RegExp.$1,
            (dateTime.getFullYear() + '').substr(4 - RegExp.$1.length),
        );
    }
    for (let k in o) {
        if (new RegExp('(' + k + ')').test(newDataStrin)) {
            newDataStrin = newDataStrin.replace(
                RegExp.$1,
                RegExp.$1.length === 1 ? o[k] : ('00' + o[k]).substr(('' + o[k]).length),
            );
        }
    }
    return newDataStrin;
};
