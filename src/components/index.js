export { default as appnavbar } from './navbar/navbar.vue'
export { default as appheader } from './header/header.vue'
export { default as appfooter } from './footer/footer.vue'
export { default as apage } from './page/page.vue'
