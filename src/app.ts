import { createApp } from 'vue'
import { showLoading, gTitle } from "../utils/global"
import './app.scss'

const App = createApp(
  {
    onLaunch() {
    },
    onShow(options) {
      // console.log(options);
      showLoading();
    },
    mounted () {
    },
    onReady(){
      console.log('ready');
    }
    // 入口组件不需要实现 render 方法，即使实现了也会被 taro 所覆盖
  })
export default App
