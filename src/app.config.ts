export default {
  pages: [
    'pages/index/index',
    'pages/area/index',
    'pages/shop/index'
  ],
  permission: {
    'scope.userLocation': {
      desc: '你的位置信息将用于小程序位置接口的效果展示'
    }
  },
  window: {
    "backgroundTextStyle": "light",
    "navigationBarBackgroundColor": "#000",
    "navigationBarTitleText": "ES-TALENT SYSTEM",
    "navigationBarTextStyle": "white",
    "navigationStyle": "custom"
  }
}
